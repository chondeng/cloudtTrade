package com.swpu.cloudtrade;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@MapperScan("com.swpu.cloudtrade.dao")
@EnableTransactionManagement//开启事务
public class CloudtradeApplication {

    public static void main(String[] args) {
        SpringApplication.run(CloudtradeApplication.class, args);
    }

}
