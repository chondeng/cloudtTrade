package com.swpu.cloudtrade.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class CrosConfig implements WebMvcConfigurer {


    public static final String[] METHOD = {"GET","POST","DELETE","PATCH","PUT"};
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOriginPatterns("*")
                .allowedHeaders("*")
                .allowedMethods(METHOD)
                .allowCredentials(false)
                .maxAge(6000);
    }



}
