package com.swpu.cloudtrade.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2  //开启Swagger2服务
public class Swagger2Config {
    @Bean //注入一个对象
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())   //接口文档的基本信息
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.swpu.cloudtrade.controller"))
                .paths(PathSelectors.any())//暴露所有接口
                .build();
    }

    //apiInfo指的是接口文档信息
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("西柚云摆摊")
                .description("西柚云摆摊的接口文档")
                .contact(new Contact("DengChuang", "https://blog.dclldc.com/", "956350481@qq.com"))
                .termsOfServiceUrl("http://localhost:8080")//进入项目主页
                .version("1.0")
                .build();
    }

}
