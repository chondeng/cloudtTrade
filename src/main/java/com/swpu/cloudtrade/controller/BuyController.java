package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.BuyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * (Buy)表控制层
 *
 * @author makejava
 * @since 2021-07-06 11:14:00
 */
@Api(tags="求购商品模块")
@RestController
@RequestMapping("buys")
public class BuyController {
    /**
     * 服务对象
     */
    @Autowired
    private BuyService buyService;

    @ApiOperation(value = "获取buy列表信息",notes = "获取buy列表信息")
    @GetMapping("/all")
    public ResponseData queryAll(Integer page,Integer limit){
        return buyService.queryAllByLimit(page,limit);
    }

    @ApiOperation(value="改变商品状态",notes = "根据bid发布状态")
    @PostMapping("/")
    public ResponseData updateStatus(@RequestBody String value){
        JSONObject jsonObject = JSONObject.parseObject(value);
        Integer id = Integer.valueOf(jsonObject.getString("id"));
        Integer status = jsonObject.getInteger("status");
        return  buyService.updateStatus(id,status);
    }

    @ApiOperation(value="删除数据",notes = "根据b_id删除数据")
    @DeleteMapping("/")
    public ResponseData delete(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray arr = jsonObject.getJSONArray("arr");
        return buyService.delete(arr);
    }
    @ApiOperation(value = "求购信息列表",notes = "查找全部发布求购商品列表以及发布人信息")
    @GetMapping("/type/")
    public ResponseData queryByBuy() {
        return  this.buyService.queryByBuy();
    }

    @PostMapping("buy")
    @ApiOperation(value = "新增一条求购信息",notes = "新增一条求购商品发布信息")
    public ResponseData addBuy(@RequestBody Buy buy) {
        return new ResponseData(buyService.save(buy) ? 1 : 0);
    }
    @ApiOperation(value = "显示我的求购信息",notes = "通过求购信息")
    @ApiImplicitParam(name = "uId",value = "u_id:用户的id")
    @GetMapping("/buy/{uId}")
    public ResponseData queryByuId(@PathVariable Integer uId)
    {

        return buyService.queryByuId(uId);
    }
    @ApiOperation(value="修改求购信息数据",notes = "根据bid修改数据,需要传入其他参数")
    @PostMapping("/change")
    public ResponseData update(@RequestBody Buy buy) {
        return  buyService.updateInfo(buy);

    }

}