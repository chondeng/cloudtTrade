package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (User)表控制层
 *
 * @author makejava
 * @since 2021-07-05 16:48:28
 */
@Api(tags = "小程序用户模块")
@RestController
@RequestMapping("users")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private UserService userService;



    @PostMapping("user")
    @ApiOperation(value = "新增用户",notes = "新增一条登录用户信息")
    public ResponseData addOne(@RequestBody User user) {
        return new ResponseData(userService.save(user) ? 1 : 0);
    }

    @ApiOperation(value = "获取用户信息",notes = "获取用户信息")
    @GetMapping("/all")
    public ResponseData queryAll(Integer page,Integer limit){
        return userService.queryAllByLimit(page,limit);
    }
    @ApiOperation(value="改变验证状态",notes = "后台管理手动对用户学生认证进行审核")
    @PatchMapping("/")
    public ResponseData updateStatus(@RequestBody String value){
        JSONObject jsonObject = JSONObject.parseObject(value);
        Integer id = Integer.valueOf(jsonObject.getString("id"));
        Integer status = jsonObject.getInteger("status");
        return  userService.updateStatus(id,status);
    }
    @ApiOperation(value="删除用户数据",notes = "根据b_id删除数据")
    @DeleteMapping("/")
    public ResponseData delete(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray arr = jsonObject.getJSONArray("arr");
        return userService.delete(arr);
    }
    @ApiOperation(value="修改用户数据",notes = "根据id修改数据,需要传入sex ,telephone,stuNumber,localtion, uId 5个参数")
    @PutMapping("/")
    public ResponseData update(@RequestBody User user) {
       return  userService.updateInfo(user);
    }
    @ApiOperation(value = "通过用户id获取用户信息", notes = "通过用户id获取用户信息")
    @ApiImplicitParam(name = "id", value = "通过用户id获取用户信息")
    @GetMapping("/query/{id}")
    public ResponseData queryById(@PathVariable Integer id) {
        return new ResponseData(ResponseCode.SUCCESS, userService.queryById(id));

    }

    @ApiOperation(value="通过id更新认证图片",notes = "uId ,picfigure图片永久链接2个参数")
    @PutMapping("/pic")
    public ResponseData uppic(@RequestBody User user) {
        return  userService.updatepic(user);
    }

    @ApiOperation(value="后台管理端修改用户数据",notes = "根据id修改数据,需要传入sex ,telephone,stuNumber,localtion, uId，nickname 6个参数")
    @PostMapping("/change")
    public ResponseData change(@RequestBody User user) {
        return  userService.change(user);
    }

}
