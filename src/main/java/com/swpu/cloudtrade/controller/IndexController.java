package com.swpu.cloudtrade.controller;


import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.BuyService;
import com.swpu.cloudtrade.service.IndexService;
import com.swpu.cloudtrade.service.ProductService;
import com.swpu.cloudtrade.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("indexs")
@Api(tags = "后台首页管理模块")
public class IndexController {

    @Resource
    private ProductService productService;

    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "总商品数",notes = "获取要售物品总数")
    @GetMapping("/procount")
    public ResponseData getProCount() {
        return productService.getProCount();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "今日新增商品数",notes = "获取今日要售物品总数")
    @GetMapping("/pcbd")
    public ResponseData getProCountByDay() {
        return productService.getProCountByDay();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "本月新增商品数",notes = "获取本月要售物品总数")
    @GetMapping("/pcbm")
    public ResponseData getProCountByMonth() {
        return productService.getProCountByMonth();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "今年新增商品总数",notes = "获取今年要售物品总数")
    @GetMapping("/pcby")
    public ResponseData getProCountByYear() {
        return productService.getProCountByYear();
    }
    @Resource
    private BuyService buyService;

    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "获取总求购数",notes = "获取求购总数")
    @GetMapping("/buycount")
    public ResponseData getGoodsCount() {
        return buyService.getCount();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "获取今日求购数",notes = "获取当日求购总数")
    @GetMapping("/cbd")
    public ResponseData getGoodsCountByDay() {
        return buyService.getGoodsCountByDay();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "获取本月求购数",notes = "获取当月求购总数")
    @GetMapping("/cbm")
    public ResponseData getGoodsCountByMonth() {
        return buyService.getGoodsCountByMonth();
    }
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "今年求购数",notes = "获取当月求购总数")
    @GetMapping("/cby")
    public ResponseData getGoodsCountByYear() {
        return buyService.getGoodsCountByYear();
    }

    @Resource
    private UserService userService;
    /**
     * 通过主键查询单条数据
     *
     */
    @ApiOperation(value = "用户总数",notes = "获取用户总数")
    @GetMapping("/usercount")
    public ResponseData getUserCount() {
        return userService.getUserCount();
    }

    @Resource
    private IndexService indexService;

    @ApiOperation(value = "获取当前时间",notes = "获取当前时间")
    @GetMapping("/time")
    public ResponseData getTime(){
        return  indexService.getTime();
    }
}
