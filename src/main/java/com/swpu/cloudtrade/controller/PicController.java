package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import com.swpu.cloudtrade.entity.Pic;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.PicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * (Pic)表控制层
 *
 * @author makejava
 * @since 2021-07-06 17:21:26
 */
@Api(tags = "图片管理模块")
@RestController
@RequestMapping("pics")
public class PicController {
    /**
     * 服务对象
     *
     * @resource:自动装配 根据名字去匹配对象
     * @Autowired 根据类型进行匹配
     */
    @Autowired
    private PicService picService;

    @Value("${aliyun.oss.endpoint}")
    private String ALIYUN_OSS_ENDPOINT;
    @Value("${aliyun.oss.accessKeyId}")
    private String ALIYUN_OSS_ACCESSKEYID;
    @Value("${aliyun.oss.accessKeySecret}")
    private String ALIYUN_OSS_ACCESSKEYSECRET;
    @Value("${aliyun.oss.bucketName}")
    private String ALIYUN_OSS_BUCKETNAME;


    @ApiOperation(value = "获取对应的轮播图片或者是导航图片", notes = "通过图片类型获取对应的轮播图片或者是导航图片")
    @ApiImplicitParam(name = "type", value = "图片的类型")
    @GetMapping("/{type}")
    public ResponseData queryByType(@PathVariable String type, HttpServletResponse response) {
        return picService.queryByType(type);
    }

    @ApiOperation(value = "获取图片信息", notes = "通过图片id获取图片信息")
    @ApiImplicitParam(name = "id", value = "图片id")
    @GetMapping("/v1/{id}")
    public ResponseData queryById(@PathVariable Integer id) {
        return new ResponseData(ResponseCode.SUCCESS, picService.queryById(id));
    }

    @ApiOperation(value = "获取轮播图片列表", notes = "获取轮播图片列表")
    @GetMapping("/lb")
    public ResponseData queryLbByLimit(Integer page, Integer limit) {

        return picService.queryLbByLimit(page, limit);
    }

    @ApiOperation(value = "获取导航图片列表", notes = "获取导航图片列表")
    @GetMapping("/nav")
    public ResponseData queryNavByLimit(Integer page, Integer limit) {

        return picService.queryNavByLimit(page, limit);
    }

    @ApiOperation(value = "修改图片的发布状态", notes = "通过图片id修改图片的发布状态")
    @PatchMapping("/")
    public ResponseData updateStatus(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        Integer id = Integer.valueOf(jsonObject.getString("id"));
        Integer status = jsonObject.getInteger("status");
        return picService.updateStatus(id, status);

    }

    @ApiOperation(value = "删除图片", notes = "通过图片id删除图片")
    @DeleteMapping("/")
    public ResponseData delete(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray arr = jsonObject.getJSONArray("arr");
        return picService.delete(arr);
    }


    @ApiOperation(value = "图片上传", notes = "图片上传通用接口，返回图片永久链接")
    @PostMapping("/")
    public ResponseData uploadFile(MultipartFile file, HttpServletRequest request) throws Exception {

        OSSClient ossClient = new OSSClient(ALIYUN_OSS_ENDPOINT, ALIYUN_OSS_ACCESSKEYID, ALIYUN_OSS_ACCESSKEYSECRET);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        // 最后上传生成的文件名
        String finalFileName = System.currentTimeMillis() + "" + new SecureRandom().nextInt(0x0400);
        // oss中的文件夹名
        String objectName = sdf.format(new Date()) + "/" + finalFileName;
        // 创建上传文件的元信息，可以通过文件元信息设置HTTP header(设置了才能通过返回的链接直接访问)。
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("image/jpg");
        // 文件上传
        ossClient.putObject(ALIYUN_OSS_BUCKETNAME, objectName, new ByteArrayInputStream(file.getBytes()), objectMetadata);
        // 设置URL过期时间为1小时。
        String url = "http://dctypore.oss-cn-beijing.aliyuncs.com/" + objectName;
        ossClient.shutdown();

        return new ResponseData("200","成功",url,0);
    }


    @PostMapping("pic")
    @ApiOperation(value = "新增图片",notes = "新增一条图片信息")
    public ResponseData addOne(@RequestBody Pic pic) {
        return new ResponseData(picService.save(pic) ? 1 : 0);
    }

}
