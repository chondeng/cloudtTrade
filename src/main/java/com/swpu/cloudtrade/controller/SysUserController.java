package com.swpu.cloudtrade.controller;

import com.swpu.cloudtrade.entity.SysUser;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.SysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


/**
 * (SysUser)表控制层
 *
 * @author makejava
 * @since 2021-07-01 11:20:01
 */
@RestController
@RequestMapping("sysUser")
@Api(tags = "系统用户模块")
public class SysUserController {

    private Logger logger= LoggerFactory.getLogger(SysUserController.class);

    @Resource
    private SysUserService sysUserService;




    /**
     * 这个就是登陆的方法
     * @param sysUser
     * @return
     */

    @RequestMapping(value = "login.action",method = RequestMethod.POST)
    @ApiOperation(value = "登陆接口")
    public ResponseData login(@RequestBody SysUser sysUser) throws Exception {

        SysUser sysUser1=sysUserService.login(sysUser);
        //接下来 将这个信息进行封装返回给客户端
        System.out.println(sysUser1);
        return new ResponseData(sysUser1);
    }


    /**
     * 查询所有用户的接口
     * @return
     */
    @RequestMapping(value = "list.action",method = RequestMethod.GET)
    @ApiOperation(value = "查询所有用户",notes = "用户的token必须传输")
    @ApiImplicitParam(name = "token",value = "用户token",dataType = "String",paramType = "header",required = true)
    public ResponseData list(){
        List<SysUser> sysUsers = sysUserService.queryAllByLimit(0, 10);
        logger.info("查询到的用户数据是:{}",sysUsers);
        return new ResponseData(sysUsers);
    }

    @ApiOperation(value = "获取用户总数",notes = "获取用户总数")
    @GetMapping("/count")
    public ResponseData getUserCount(){
        return sysUserService.getCount();
    }



}
