package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.kevinsawicki.http.HttpRequest;
import com.swpu.cloudtrade.config.WxMiniConfig;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@RestController
@Api(tags = "小程序授权登录接口")
public class WxOpenData {


    @Resource
    private UserService userService;

    @PostMapping("/WxOpenData")
    @ApiOperation(value = "小程序用户登录接口",notes = "601：code错误未获取到openid|602：未获得用户授权信息，返回openid|200:已获得用户授权信息，返回用户所有信息")
    public ResponseData getWxOpenData(@RequestParam(value = "js_code", required = true) String js_code) {
        Map<String, String> data = new HashMap<String, String>();
        data.put("appid", WxMiniConfig.AppId);
        data.put("secret", WxMiniConfig.AppSecret);
        data.put("js_code", js_code);
        data.put("grant_type", "authorization_code");

        String response = HttpRequest.get("https://api.weixin.qq.com/sns/jscode2session").form(data).body();

        JSONObject obj = JSONObject.parseObject(response);//将json字符串转换为json对

        String openid = obj.getString("openid");
        if (openid == null || openid == "") {
            return new ResponseData("601", "未获取到openid");
        } else {
            User user = userService.findByOpenid(openid);
            if (user == null) {
                return new ResponseData("602", "未授权获得用户信息登录，请授权",openid,1);
            } else {
                return new ResponseData(ResponseCode.SUCCESS, user);
            }
        }

    }

}
