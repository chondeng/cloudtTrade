package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.entity.Feedback;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.FeedbackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * (Feedback)表控制层
 *
 * @author makejava
 * @since 2021-07-09 10:35:38
 */
@Api(tags="意见反馈模块")
@RestController
@RequestMapping("feedback")
public class FeedbackController {
    /**
     * 服务对象
     */
    @Resource
    private FeedbackService feedbackService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    @ApiOperation(value = "查询",notes = "查询一条反馈意见信息")
    public Feedback selectOne(Integer id) {
        return this.feedbackService.queryById(id);
    }

    @PostMapping("addOne")
    @ApiOperation(value = "新增反馈意见",notes = "新增一条反馈意见信息")
    public ResponseData addOne(@RequestBody Feedback feedback) {
        return new ResponseData(feedbackService.save(feedback) ? 1 : 0);
    }

    @ApiOperation(value = "获取反馈信息列表", notes = "获取反馈信息列表")
    @GetMapping("/all")
    public ResponseData queryAll(Integer page, Integer limit) {

        return feedbackService.queryAllByLimit(page, limit);
    }

    @ApiOperation(value = "删除反馈信息", notes = "通过图片id删除反馈信息")
    @DeleteMapping("/del")
    public ResponseData delete(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray arr = jsonObject.getJSONArray("arr");
        return feedbackService.delete(arr);
    }
}