package com.swpu.cloudtrade.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.Product;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;


/**
 * (Product)表控制层
 *
 * @author makejava
 * @since 2021-07-06 09:48:49
 */
@Api(tags = "商品发布模块")
@RestController
@RequestMapping("products")
public class ProductController {
    /**
     * 服务对象
     */
    @Resource
    private ProductService productService;

    @ApiOperation(value = "获取buy列表信息",notes = "获取buy列表信息")
    @GetMapping("/all")
    public ResponseData queryAll(Integer page,Integer limit){
        return productService.queryAllByLimit(page,limit);
    }

    @PostMapping("product")
    @ApiOperation(value = "新增一条记录",notes = "新增一条商品发布信息")
    public ResponseData addOne(@RequestBody Product product) {
        return new ResponseData(productService.save(product) ? 1 : 0);
    }

    @ApiOperation(value = "小程序首页商品发布列表",notes = "根据类型查询列表，参数为(all)查找全部发布商品")
    @ApiImplicitParam(name = "title",value = "类别")
    @GetMapping("/type/{title}")
    public ResponseData queryByTitle(@PathVariable String title) {
        return  this.productService.queryByTitle(title);
    }





    @ApiOperation(value = "显示我的发布商品信息",notes = "通过u_id显示发布商品信息")
    @ApiImplicitParam(name = "uId",value = "u_id:用户的id")
    @GetMapping("/release/{uId}")
    public ResponseData queryByuId(@PathVariable Integer uId)
    {

        return productService.queryByuId(uId);
    }

    @ApiOperation(value="改变商品状态",notes = "根据pro_id修改商品的售出状态")
    @PostMapping("/status")
    public ResponseData updateStatus(@RequestBody String value){
        //后台如何接收json数据  1.实体类映射  2.解析字符串
        JSONObject jsonObject = JSONObject.parseObject(value);
        Integer id = Integer.valueOf(jsonObject.getString("id"));
        Integer status = jsonObject.getInteger("status");
        return  productService.updateStatus(id,status);
    }



    @ApiOperation(value = "实现模糊查询",notes = "实现模糊查询")
   @ApiImplicitParam(name = "productName",value = "productName")
    @GetMapping("/search/{productName}")
    public ResponseData getProductByKey(@PathVariable String productName)
    {

       return this.productService.getProductByKey(productName);
    }

    @ApiOperation(value = "获取商品详细信息", notes = "通过proId获取商品详细信息")
    @ApiImplicitParam(name = "proId", value = "商品id")
    @GetMapping("/v1/{proId}")
    public ResponseData queryById(@PathVariable Integer proId) {
        return new ResponseData(ResponseCode.SUCCESS, productService.queryById(proId));
    }
    @ApiOperation(value="删除数据",notes = "根据pro_id删除数据")
    @DeleteMapping("/")
    public ResponseData delete(@RequestBody String value) {
        JSONObject jsonObject = JSONObject.parseObject(value);
        JSONArray arr = jsonObject.getJSONArray("arr");
        return productService.delete(arr);
    }


}


