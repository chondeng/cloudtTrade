package com.swpu.cloudtrade.exception;


import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.result.ResponseCode;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@Component
public class GlobleException implements HandlerExceptionResolver {
    @Override
    public ModelAndView resolveException(HttpServletRequest req, HttpServletResponse resp, Object o, Exception e) {

        JSONObject jsonObject = new JSONObject();
        //处理各种异常
        if (e instanceof BusinessException) {
            BusinessException businessException = (BusinessException) e;
            jsonObject.put("code", businessException.getCode());
            jsonObject.put("errorinfo", businessException.getMessage());
        } else if (e instanceof SQLException) {
            jsonObject.put("code", ResponseCode.SQL_EXCEPTION.getEcode());
            jsonObject.put("errorinfo", ResponseCode.SQL_EXCEPTION.getErrorinfo());
        } else {
            jsonObject.put("code", ResponseCode.SYSTEM_EXCEPTION.getEcode());
            jsonObject.put("errorinfo", ResponseCode.SYSTEM_EXCEPTION.getErrorinfo());
        }
        //将数据返回客户端
        resp.setContentType("text/html;charset=utf-8");
        try {
            resp.getWriter().write(jsonObject.toString());
            resp.getWriter().flush();
            resp.getWriter().close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }
}
