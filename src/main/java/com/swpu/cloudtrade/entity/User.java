package com.swpu.cloudtrade.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2021-07-05 16:48:28
 */
@Data
public class User implements Serializable {
    /**
     * 用户编号
     */
    private Integer uId;
    /**
     * 微信用户身份id
     */
    private String openid;
    /**
     * 微信名
     */
    private String nickname;
    /**
     * 性别
     */
    private String sex;
    /**
     * 微信头像链接
     */
    private String avatar;
    /**
     * 学号
     */
    private String stuNumber;
    /**
     * 手机号
     */
    private String telephone;
    /**
     * 联系地址
     */
    private String localtion;

    private String picfigure;

    private Boolean isfigure;

}
