package com.swpu.cloudtrade.entity;

import lombok.Data;

import java.util.Date;
import java.io.Serializable;

/**
 * (Buy)实体类
 *
 * @author makejava
 * @since 2021-07-06 11:14:00
 */
@Data
public class Buy implements Serializable {
    private static final long serialVersionUID = 809622208875263010L;
    /**
     * 求购信息的id
     */
    private Integer bId;
    /**
     * 用户id
     */
    private Integer uId;
    /**
     * 微信名称
     */
    private String nickname;
    /**
     * 求购者联系方式
     */
    private String telephone;
    /**
     * 求购类型  如：书籍资料、生活用品等
     */
    private String bType;
    /**
     * 求购价格
     */
    private Integer bPrice;
    /**
     * 商品名字
     */
    private String bName;
    /**
     * 描述求购物品信息
     */
    private String bDetail;
    /**
     * 求购状态： 1 求购中 0 放弃求购
     */
    private Boolean bStatus;
    /**
     * 发布时间
     */
    private Date createTime;

    private  User user;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public void setbId(Integer bId) {
        this.bId = bId;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getbType() {
        return bType;
    }

    public void setbType(String bType) {
        this.bType = bType;
    }

    public Integer getbPrice() {
        return bPrice;
    }

    public void setbPrice(Integer bPrice) {
        this.bPrice = bPrice;
    }

    public String getbName() {
        return bName;
    }

    public void setbName(String bName) {
        this.bName = bName;
    }

    public String getbDetail() {
        return bDetail;
    }

    public void setbDetail(String bDetail) {
        this.bDetail = bDetail;
    }

    public Boolean getbStatus() {
        return bStatus;
    }

    public void setbStatus(Boolean bStatus) {
        this.bStatus = bStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getbId() {
        return bId;
    }
}