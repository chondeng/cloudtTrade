package com.swpu.cloudtrade.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * (Product)实体类
 *
 * @author makejava
 * @since 2021-07-06 09:48:49
 */
@Data
@ApiModel("商品实体")
public class Product implements Serializable {
    private static final long serialVersionUID = -71341308441887352L;
    /**
     * 商品id
     */
    private Integer proId;
    /**
     * 是否免费
     */
    @ApiModelProperty(value = "是否免费",required = true)
    private Boolean proIsfree;
    /**
     * 商品名称
     * 商品名称
     */
    @ApiModelProperty(value = "商品名",required = true)
    private String proName;
    /**
     * 商品描述
     */
    @ApiModelProperty(value = "商品描述",required = true)
    private String proDesc;
    /**
     * 商品价格
     */
    @ApiModelProperty(value = "商品价格",required = true)
    private int proPrice;
    /**
     * 交易地点
     */
    @ApiModelProperty(value = "交易地点",required = true)
    private String proPlace;
    /**
     * 唯一商品图片地址
     */
    @ApiModelProperty(value = "唯一商品图片地址",required = true)
    private String proImg;
    /**
     * 发布者联系电话
     */
    @ApiModelProperty(value = "发布者联系电话",required = true)
    private String telephone;
    /**
     * 发布人的用户编号
     */
    private Integer uId;
    /**
     * 商品状态
     */
    private Boolean proStatus = true;

    /**
     * 商品类别
     */
    @ApiModelProperty(value = "商品类别",required = true)
    private String pType;
    /**
     * 发布时间
     */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",  timezone = "GMT+8")
    private Date createTime;


    private  User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getProId() {
        return proId;
    }

    public void setProId(Integer proId) {
        this.proId = proId;
    }

    public Boolean getProIsfree() {
        return proIsfree;
    }

    public void setProIsfree(Boolean proIsfree) {
        this.proIsfree = proIsfree;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProDesc() {
        return proDesc;
    }

    public void setProDesc(String proDesc) {
        this.proDesc = proDesc;
    }

    public int getProPrice() {
        return proPrice;
    }

    public void setProPrice(int proPrice) {
        this.proPrice = proPrice;
    }

    public String getProPlace() {
        return proPlace;
    }

    public void setProPlace(String proPlace) {
        this.proPlace = proPlace;
    }

    public String getProImg() {
        return proImg;
    }

    public void setProImg(String proImg) {
        this.proImg = proImg;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public Boolean getProStatus() {
        return proStatus;
    }

    public void setProStatus(Boolean proStatus) {
        this.proStatus = proStatus;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
