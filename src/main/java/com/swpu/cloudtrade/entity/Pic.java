package com.swpu.cloudtrade.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * (Pic)实体类
 *
 * @author makejava
 * @since 2021-07-06 17:21:26
 */

public class Pic implements Serializable {

    /**
    * 图片id
    */
    private Integer pId;
    /**
    * 图片标题
    */
    private String pTitle;
    /**
    * 图片地址
    */
    private String pUrl;
    /**
    * 图片类型：用途
    */
    private String pType;
    /**
    * 图片状态
    */
    private Integer pStatus;

    public Integer getpId() {
        return pId;
    }

    public void setpId(Integer pId) {
        this.pId = pId;
    }

    public String getpTitle() {
        return pTitle;
    }

    public void setpTitle(String pTitle) {
        this.pTitle = pTitle;
    }

    public String getpUrl() {
        return pUrl;
    }

    public void setpUrl(String pUrl) {
        this.pUrl = pUrl;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public Integer getpStatus() {
        return pStatus;
    }

    public void setpStatus(Integer pStatus) {
        this.pStatus = pStatus;
    }
}