package com.swpu.cloudtrade.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Feedback)实体类
 *
 * @author makejava
 * @since 2021-07-09 10:35:38
 */
public class Feedback implements Serializable {
    private static final long serialVersionUID = -80635673878371684L;
    /**
    * 反馈编号
    */
    private Integer fId;
    /**
    * 反馈内容
    */
    private String fDetail;
    /**
    * 用户id
    */
    private Integer uId;
    /**
    * 微信名称
    */
    private String nickname;
    /**
    * 电话
    */
    private String telephone;
    /**
    * 提交时间
    */
    private Date createTime;


    public Integer getfId() {
        return fId;
    }

    public void setfId(Integer fId) {
        this.fId = fId;
    }

    public String getfDetail() {
        return fDetail;
    }

    public void setfDetail(String fDetail) {
        this.fDetail = fDetail;
    }

    public Integer getuId() {
        return uId;
    }

    public void setuId(Integer uId) {
        this.uId = uId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}