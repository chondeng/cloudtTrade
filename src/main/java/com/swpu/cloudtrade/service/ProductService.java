package com.swpu.cloudtrade.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.Product;
import com.swpu.cloudtrade.result.ResponseData;

import java.util.List;

/**
 * (Product)表服务接口
 *
 * @author makejava
 * @since 2021-07-06 09:48:49
 */
public interface ProductService  extends IService<Product> {

    /**
     * 通过ID查询单条数据
     *
     * @param proId 主键
     * @return 实例对象
     */
    Product queryById(Integer proId);
    ResponseData queryByuId(Integer uId);
    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    ResponseData queryAllByLimit(Integer offset, Integer limit);

    /**
     * 模糊查询
     */
    ResponseData getProductByKey(String productName);
    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 实例对象
     */


    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    Product update(Product product);

    /**
     * 通过主键删除数据
     *
     * @param proId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer proId);

    ResponseData getProCount();

    ResponseData getProCountByDay();

    ResponseData getProCountByMonth();

    ResponseData getProCountByYear();

    /**
     * 通过分类来获取对应的数据列表
     * @param title
     * @return
     */
    ResponseData queryByTitle(String title);


    ResponseData updateStatus(Integer id, Integer status);


    ResponseData delete(JSONArray arr);
}
