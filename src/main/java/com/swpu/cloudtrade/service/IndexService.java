package com.swpu.cloudtrade.service;

import com.swpu.cloudtrade.result.ResponseData;

public interface IndexService {

    ResponseData getTime();
}
