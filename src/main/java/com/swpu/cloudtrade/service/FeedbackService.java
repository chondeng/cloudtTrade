package com.swpu.cloudtrade.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swpu.cloudtrade.entity.Feedback;
import com.swpu.cloudtrade.result.ResponseData;

import java.util.List;

/**
 * (Feedback)表服务接口
 *
 * @author makejava
 * @since 2021-07-09 10:35:38
 */
public interface FeedbackService extends IService<Feedback> {

    /**
     * 通过ID查询单条数据
     *
     * @param fId 主键
     * @return 实例对象
     */
    Feedback queryById(Integer fId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Feedback> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    Feedback insert(Feedback feedback);

    /**
     * 修改数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    Feedback update(Feedback feedback);

    /**
     * 通过主键删除数据
     *
     * @param fId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer fId);

    ResponseData queryAllByLimit(Integer page, Integer limit);

    ResponseData delete(JSONArray arr);
}