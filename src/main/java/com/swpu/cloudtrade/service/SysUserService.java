package com.swpu.cloudtrade.service;

import com.swpu.cloudtrade.entity.SysUser;
import com.swpu.cloudtrade.result.ResponseData;

import java.util.List;

/**
 * (SysUser)表服务接口
 *
 * @author makejava
 * @since 2021-07-06 11:00:40
 */
public interface SysUserService {

    /**
     * 通过ID查询单条数据
     *
     * @param sId 主键
     * @return 实例对象
     */
    SysUser queryById(Integer sId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<SysUser> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param sysUser 实例对象
     * @return 实例对象
     */
    SysUser insert(SysUser sysUser);

    /**
     * 修改数据
     *
     * @param sysUser 实例对象
     * @return 实例对象
     */
    SysUser update(SysUser sysUser);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer uId);

    /**
     * 获取用户的总数量
     * @return
     */
    ResponseData getCount();


    /**
     * 登陆的方法
     * @param sysUser
     * @return
     */
    SysUser login(SysUser sysUser)throws Exception;


    /**
     * 通过token去找咋们的用户
     * @param token
     * @return
     * @throws Exception
     */
    SysUser findUserByToken(String token)throws Exception;
}