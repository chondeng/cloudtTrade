package com.swpu.cloudtrade.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseData;

/**
 * (User)表服务接口
 *
 * @author makejava
 * @since 2021-07-05 16:48:28
 */
public interface UserService extends IService<User> {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    User queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    ResponseData queryAllByLimit(Integer offset, Integer limit);

    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User insert(User user);

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    User update(User user);

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer uId);


    User findAll();

    ResponseData getUserCount();


    ResponseData updateStatus(Integer id, Integer status);

    ResponseData delete(JSONArray arr);

    ResponseData updateInfo(User user);


    ResponseData updatepic(User user);


    ResponseData change(User user);

    User findByOpenid(String openid);

}
