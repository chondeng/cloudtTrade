package com.swpu.cloudtrade.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swpu.cloudtrade.dao.UserDao;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * (User)表服务实现类
 *
 * @author makejava
 * @since 2021-07-05 16:48:28
 */
@Service("userService")
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {
    @Resource
    private UserDao userDao;

    /**
     * 通过ID查询单条数据
     *
     * @param uId 主键
     * @return 实例对象
     */
    @Override
    public User queryById(Integer uId) {
        return this.userDao.queryById(uId);
    }

    @Override
    public ResponseData queryAllByLimit(Integer offset, Integer limit) {
        if (offset == null && limit == null) {
            offset = 0;
            limit = 10;
        } else {
            offset = (offset - 1) * limit;
        }
        List<User> list = this.userDao.queryAllByLimit(offset, limit);
        Integer i = userDao.getCount();
        return new ResponseData("0", "success", list, i);
    }


    /**
     * 新增数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User insert(User user) {
        this.userDao.insert(user);
        return user;
    }

    /**
     * 修改数据
     *
     * @param user 实例对象
     * @return 实例对象
     */
    @Override
    public User update(User user) {
        this.userDao.update(user);
        return this.queryById(null);
    }

    /**
     * 通过主键删除数据
     *
     * @param uId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer uId) {
        return this.userDao.deleteById(uId) > 0;
    }

    @Override
    public User findAll() {
        return userDao.findAll();
    }

    @Override
    public ResponseData getUserCount() {
        Integer i = userDao.getUserCount();
        return new ResponseData(ResponseCode.SUCCESS, i);
    }

    @Override
    public ResponseData updateStatus(Integer id, Integer status) {
        //修改
        //先去数据库判定当前数据是否存在
        try {
            Integer i = userDao.updateStatus(id, status);
            if (i > 0) {
                return new ResponseData(ResponseCode.SUCCESS);
            } else {
                return new ResponseData(ResponseCode.FAILED);
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData(ResponseCode.FAILED);
        }
    }

    @Override
    public ResponseData delete(JSONArray arr) {
        try {
            for (int i = 0; i < arr.size(); i++) {
                userDao.deleteById((Integer) arr.get(i));
                return new ResponseData((ResponseCode.SUCCESS));
            }
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData((ResponseCode.FAILED));

        }
        return null;
    }

    @Override
    public ResponseData updateInfo(User user) {
        int i = userDao.updateInfo(user);
        if (i == 1) {
            return new ResponseData(ResponseCode.SUCCESS);
        } else return new ResponseData(ResponseCode.FAILED);
    }

    @Override
    public ResponseData updatepic(User user) {
        int i = userDao.updatepic(user);
        if (i == 1) {
            return new ResponseData(ResponseCode.SUCCESS);
        } else return new ResponseData(ResponseCode.FAILED);
    }

    @Override
    public ResponseData change(User user) {
        int i = userDao.change(user);
        if (i == 1) {
            return new ResponseData(ResponseCode.SUCCESS);
        } else return new ResponseData(ResponseCode.FAILED);
    }

    @Override
    public User findByOpenid(String openid) {

        return userDao.findByOpenid(openid);
    }


}
