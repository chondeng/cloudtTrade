package com.swpu.cloudtrade.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swpu.cloudtrade.dao.ProductDao;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.Product;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.ProductService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * (Product)表服务实现类
 *
 * @author makejava
 * @since 2021-07-06 10:52:33
 */
@Service("productService")
public class ProductServiceImpl extends ServiceImpl<ProductDao,Product> implements ProductService {
    @Resource
    private ProductDao productDao;

    /**
     * 通过ID查询单条数据
     *
     * @param proId 主键
     * @return 实例对象
     */
    @Override
    public Product queryById(Integer proId) {
        //实现对业务的逻辑判定
        //调用dao数据操作层
        return this.productDao.queryById(proId);
    }

    @Override
    public ResponseData  queryByuId(Integer uId) {
        List<Product> products = productDao.queryByuId(uId);
        if(null==products||products.size()==0){
            return new ResponseData(ResponseCode.NO_DATA);
        }
        return new ResponseData(ResponseCode.SUCCESS,this.productDao.queryByuId(uId));
    }

    @Override
    public ResponseData queryAllByLimit(Integer offset, Integer limit) {

        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Product> list = this.productDao.queryAllByLimit(offset, limit);
        Integer i =  productDao.getCount();
        return  new ResponseData("0","success",list,i);

    }

    @Override
    public ResponseData getProductByKey(String productName) {
        List<Product> proList1 = productDao.getProductByKey("%"+productName+"%");
        return  new ResponseData(ResponseCode.SUCCESS,proList1);

    }


    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */


    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 实例对象
     */


    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 实例对象
     */
    @Override
    public Product update(Product product) {
        this.productDao.update(product);
        return this.queryById(product.getProId());
    }

    /**
     * 通过主键删除数据
     *
     * @param proId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer proId) {
        return this.productDao.deleteById(proId) > 0;
    }

    @Override
    public ResponseData getProCount() {
        Integer i = productDao.getProCount();
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public ResponseData getProCountByDay() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String subtime = simpleDateFormat.format(date);
        Integer i = productDao.getProCountByDay(subtime+"%");
        HashMap<String,Object> map=new HashMap<>();
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public ResponseData getProCountByMonth() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String subtime = simpleDateFormat.format(date);
        Integer i = productDao.getProCountByDay(subtime+"%");
        return new ResponseData(ResponseCode.SUCCESS,i);
    }
    @Override
    public ResponseData getProCountByYear() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String subtime = simpleDateFormat.format(date);
        Integer i = productDao.getProCountByDay(subtime+"%");
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public ResponseData queryByTitle(String title) {

            List<Product>  products = productDao.queryByTitle(title);
            if(null==products||products.size()==0){
                return new ResponseData(ResponseCode.NO_DATA);
            }
            return new ResponseData(ResponseCode.SUCCESS,products);
    }

    @Override
    @Transactional
    public ResponseData updateStatus(Integer id, Integer status) {
        try{
            Integer i = productDao.updateStatus(id,status);
            if(i>0)
            {
                return new ResponseData(ResponseCode.SUCCESS);
            }else{
                return new ResponseData(ResponseCode.FAILED);
            }

        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData(ResponseCode.FAILED);

        }
    }

    @Override
    public ResponseData delete(JSONArray arr) {
        try{
            for (int i = 0; i < arr.size(); i++) {
                productDao.deleteById((Integer) arr.get(i));
                return new ResponseData((ResponseCode.SUCCESS));
            }
        }catch(Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData((ResponseCode.FAILED));

        }
        return null;
    }


}
