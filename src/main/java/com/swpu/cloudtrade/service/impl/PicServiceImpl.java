package com.swpu.cloudtrade.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swpu.cloudtrade.dao.UserDao;
import com.swpu.cloudtrade.entity.Pic;
import com.swpu.cloudtrade.dao.PicDao;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.PicService;
import com.swpu.cloudtrade.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Pic)表服务实现类
 *
 * @author makejava
 * @since 2021-07-06 17:21:26
 */
@Service("picService")
public class PicServiceImpl extends ServiceImpl<PicDao, Pic> implements PicService {
    @Resource
    private PicDao picDao;

    /**
     * 通过ID查询单条数据
     *
     * @param pId 主键
     * @return 实例对象
     */
    @Override
    public Pic queryById(Integer pId) {
        //实现对业务的逻辑判定
        //调用dao数据操作层
        return this.picDao.queryById(pId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public ResponseData queryAllByLimit(Integer offset, Integer limit) {
        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Pic> list = this.picDao.queryAllByLimit(offset, limit);
        Integer i =  picDao.getCount();
        return  new ResponseData("0","success",list,i);
    }
    /**
     * 新增数据
     *i
     * @param pic 实例对象
     * @return 实例对象
     */
    @Override
    public Pic insert(Pic pic) {
        this.picDao.insert(pic);
        return pic;
    }

    /**
     * 修改数据
     *
     * @param pic 实例对象
     * @return 实例对象
     */
    @Override
    public Pic update(Pic pic) {
        this.picDao.update(pic);
        return this.queryById(pic.getpId());
    }

    /**
     * 通过主键删除数据
     *
     * @param pId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer pId) {
        return this.picDao.deleteById(pId) > 0;
    }

    @Override
    public ResponseData queryByType(String type) {
        List<Pic> pics = picDao.queryByType(type);
        return new ResponseData(ResponseCode.SUCCESS,pics);
    }

    @Transactional
    @Override
    public ResponseData updateStatus(Integer id, Integer status) {
        //直接去进行修改
        //先去数据库判定当前数据是否存
        try{
            Integer i = picDao.updateStatus(id,status);
            if(i >0){
                return new ResponseData(ResponseCode.SUCCESS);
            }
            return new ResponseData(ResponseCode.FAILED);
        }catch (Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData(ResponseCode.FAILED);
        }



    }

    @Transactional
    @Override
    public ResponseData delete(JSONArray arr) {
        try {
            for (int i = 0; i < arr.size(); i++) {
                picDao.deleteById((Integer) arr.get(i));
            }
            return  new ResponseData(ResponseCode.SUCCESS);
        }catch(Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData(ResponseCode.FAILED);
        }

    }

    @Override
    public ResponseData queryNavByLimit(Integer offset, Integer limit) {
        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Pic> list = this.picDao.queryNavByLimit(offset, limit);
        Integer i =  picDao.getNavCount();
        return  new ResponseData("0","success",list,i);
    }

    @Override
    public ResponseData queryLbByLimit(Integer offset, Integer limit) {
        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Pic> list = this.picDao.queryLbByLimit(offset, limit);
        Integer i =  picDao.getLbCount();
        return  new ResponseData("0","success",list,i);
    }


}