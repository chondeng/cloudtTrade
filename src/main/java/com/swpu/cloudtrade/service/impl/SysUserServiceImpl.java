package com.swpu.cloudtrade.service.impl;

import com.swpu.cloudtrade.entity.SysUser;
import com.swpu.cloudtrade.dao.SysUserDao;
import com.swpu.cloudtrade.exception.BusinessException;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.SysUserService;
import org.springframework.stereotype.Service;
import com.swpu.cloudtrade.util.TokenUtil;

import javax.annotation.Resource;
import java.util.List;

/**
 * (SysUser)表服务实现类
 *
 * @author makejava
 * @since 2021-07-06 11:00:40
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {
    @Resource
    private SysUserDao sysUserDao;

    /**
     * 通过ID查询单条数据
     *
     * @param sId 主键
     * @return 实例对象
     */
    @Override
    public SysUser queryById(Integer sId) {
        return this.sysUserDao.queryById(sId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<SysUser> queryAllByLimit(int offset, int limit) {
        return this.sysUserDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param sysUser 实例对象
     * @return 实例对象
     */
    @Override
    public SysUser insert(SysUser sysUser) {
        this.sysUserDao.insert(sysUser);
        return sysUser;
    }

    /**
     * 修改数据
     *
     * @param sysUser 实例对象
     * @return 实例对象
     */
    @Override
    public SysUser update(SysUser sysUser) {
        this.sysUserDao.update(sysUser);
        return this.queryById(sysUser.getsId());
    }

    /**
     * 通过主键删除数据
     *
     * @param sId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer sId) {
        return this.sysUserDao.deleteById(sId) > 0;
    }

    @Override
    public ResponseData getCount() {
        Integer i = sysUserDao.getCount();
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public SysUser login(SysUser sysUser) throws Exception {
        if(null==sysUser.getUsername()||null==sysUser.getPassword()){
            throw new BusinessException(ResponseCode.PARAMETER_EXCEPTION.getEcode(),ResponseCode.PARAMETER_EXCEPTION.getErrorinfo());
        }
        //执行到这里 说明我们的用户参数是正常的
        //那么我们就开始通过用户名找用户对象
        SysUser sysUser1=sysUserDao.findUserByName(sysUser.getUsername());
        if(null==sysUser1){  //说明通过名字没有找到对象
            throw new BusinessException(ResponseCode.USERNAME_EXCEPTION.getEcode(),ResponseCode.USERNAME_EXCEPTION.getErrorinfo());
        }
        //接下来比较密码是否一致
        if(!sysUser.getPassword().equals(sysUser1.getPassword())){
            throw new BusinessException(ResponseCode.PASSWORD_EXCEPTION.getEcode(),ResponseCode.PASSWORD_EXCEPTION.getErrorinfo());
        }
        //执行这里 说明用户身份是正确的
        String token = TokenUtil.getToken();
        sysUser1.setToken(token);
        //接下来通过用户id  将token信息 更新进去
        sysUserDao.updateTokenById(sysUser1);
        //程序执行到这里说明token已经更新好了
        //置空这个用户密码
        sysUser1.setPassword("");
        return sysUser1;
    }

    @Override
    public SysUser findUserByToken(String token) throws Exception {
        //这个就是通过token找用户
        SysUser sysUser=sysUserDao.findUserByToken(token);
        return sysUser;
    }
}