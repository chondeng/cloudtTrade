package com.swpu.cloudtrade.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swpu.cloudtrade.dao.BuyDao;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.BuyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * (Buy)表服务实现类
 *
 * @author makejava
 * @since 2021-07-06 11:14:00
 */
@Service("buyService")
public class BuyServiceImpl extends ServiceImpl<BuyDao,Buy> implements BuyService {
    @Resource
    private BuyDao buyDao;

    @Override
    public ResponseData getCount() {
        Integer i = buyDao.getCount();
        return new ResponseData(ResponseCode.SUCCESS,i);

    }

    @Override
    public ResponseData getGoodsCountByDay() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String subtime = simpleDateFormat.format(date);
        Integer i = buyDao.getGoodsCountByDay(subtime+"%");
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public ResponseData getGoodsCountByMonth() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        String subtime = simpleDateFormat.format(date);
        Integer i = buyDao.getGoodsCountByDay(subtime+"%");
        return new ResponseData(ResponseCode.SUCCESS,i);
    }

    @Override
    public ResponseData getGoodsCountByYear() {
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String subtime = simpleDateFormat.format(date);
        Integer i = buyDao.getGoodsCountByDay(subtime+"%");
        return new ResponseData(ResponseCode.SUCCESS,i);
    }
    @Transactional
    @Override
    public ResponseData updateStatus(Integer id, Integer status) {
            //修改
            //先去数据库判定当前数据是否存在
            try{
                Integer i = buyDao.updateStatus(id,status);
                if(i>0)
                {
                    return new ResponseData(ResponseCode.SUCCESS);
                }else{
                    return new ResponseData(ResponseCode.FAILED);
                }
            }catch (Exception e){
                e.printStackTrace();
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return new ResponseData(ResponseCode.FAILED);
            }
    }

    @Override
    public ResponseData delete(JSONArray arr) {
        try{
            for (int i = 0; i < arr.size(); i++) {
                buyDao.deleteById((Integer) arr.get(i));
                return new ResponseData((ResponseCode.SUCCESS));
            }
        }catch(Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData((ResponseCode.FAILED));

        }
        return null;
    }

    @Override
    public ResponseData queryByBuy() {
        List<Buy>  buys = buyDao.queryByBuy();
        if(null==buys||buys.size()==0){
            return new ResponseData(ResponseCode.NO_DATA);
        }
        return new ResponseData(ResponseCode.SUCCESS,buys);
    }

    @Override
    public ResponseData updateInfo(Buy buy) {
        int i = buyDao.updateInfo(buy);
        if (i == 1) {
            return new ResponseData(ResponseCode.SUCCESS);
        } else return new ResponseData(ResponseCode.FAILED);
    }

    /**
     * 通过ID查询单条数据
     *
     * @param bId 主键
     * @return 实例对象
     */
    @Override
    public Buy queryById(Integer bId) {
        return this.buyDao.queryById(bId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public ResponseData queryAllByLimit(Integer offset, Integer limit) {
        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Buy> list = this.buyDao.queryAllByLimit(offset, limit);
        Integer i =  buyDao.getCount();
        return  new ResponseData("0","success",list,i);
    }

    @Override
    public ResponseData queryByuId(Integer uId) {
        List<Buy> buys = buyDao.queryByuId(uId);
        if(null==buys||buys.size()==0){
            return new ResponseData(ResponseCode.NO_DATA);
        }
        return new ResponseData(ResponseCode.SUCCESS,this.buyDao.queryByuId(uId));
    }

    /**
     * 新增数据
     *
     * @param buy 实例对象
     * @return 实例对象
     */
    @Override
    public Buy insert(Buy buy) {
        this.buyDao.insert(buy);
        return buy;
    }

    /**
     * 修改数据
     *
     * @param buy 实例对象
     * @return 实例对象
     */
    @Override
    public Buy update(Buy buy) {
        this.buyDao.update(buy);
        return this.queryById(buy.getbId());
    }

    /**
     * 修改数据
     *
     * @param buy 实例对象
     * @return 实例对象
     */


    /**
     * 通过主键删除数据
     *
     * @param bId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer bId) {
        return this.buyDao.deleteById(bId) > 0;
    }
}
