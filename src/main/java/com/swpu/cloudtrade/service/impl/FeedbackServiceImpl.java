package com.swpu.cloudtrade.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.swpu.cloudtrade.dao.UserDao;
import com.swpu.cloudtrade.entity.Feedback;
import com.swpu.cloudtrade.dao.FeedbackDao;
import com.swpu.cloudtrade.entity.Pic;
import com.swpu.cloudtrade.entity.User;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;
import com.swpu.cloudtrade.service.FeedbackService;
import com.swpu.cloudtrade.service.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.List;

/**
 * (Feedback)表服务实现类
 *
 * @author makejava
 * @since 2021-07-09 10:35:38
 */
@Service("feedbackService")
public class FeedbackServiceImpl extends ServiceImpl<FeedbackDao, Feedback> implements FeedbackService {
    @Resource
    private FeedbackDao feedbackDao;

    /**
     * 通过ID查询单条数据
     *
     * @param fId 主键
     * @return 实例对象
     */
    @Override
    public Feedback queryById(Integer fId) {
        return this.feedbackDao.queryById(fId);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Override
    public List<Feedback> queryAllByLimit(int offset, int limit) {
        return this.feedbackDao.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    @Override
    public Feedback insert(Feedback feedback) {
        this.feedbackDao.insert(feedback);
        return feedback;
    }

    /**
     * 修改数据
     *
     * @param feedback 实例对象
     * @return 实例对象
     */
    @Override
    public Feedback update(Feedback feedback) {
        this.feedbackDao.update(feedback);
        return this.queryById(feedback.getfId());
    }

    /**
     * 通过主键删除数据
     *
     * @param fId 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer fId) {
        return this.feedbackDao.deleteById(fId) > 0;
    }


    @Override
    public ResponseData queryAllByLimit(Integer offset, Integer limit) {
        if(offset == null && limit == null){
            offset = 0;
            limit =10;
        }else{
            offset = (offset-1)*limit;
        }
        List<Feedback> list = this.feedbackDao.queryAllByLimit(offset, limit);
        Integer i =  feedbackDao.getCount();
        return  new ResponseData("0","success",list,i);
    }

    @Transactional
    @Override
    public ResponseData delete(JSONArray arr) {
        try {
            for (int i = 0; i < arr.size(); i++) {
                feedbackDao.deleteById((Integer) arr.get(i));
            }
            return  new ResponseData(ResponseCode.SUCCESS);
        }catch(Exception e){
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new ResponseData(ResponseCode.FAILED);
        }

    }
}