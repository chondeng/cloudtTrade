package com.swpu.cloudtrade.service;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.Product;
import com.swpu.cloudtrade.result.ResponseData;

import java.util.List;

/**
 * (Buy)表服务接口
 *
 * @author makejava
 * @since 2021-07-06 11:14:00
 */
public interface BuyService extends IService<Buy> {


    

    /**
     * 通过ID查询单条数据
     *
     * @param bId 主键
     * @return 实例对象
     */
    Buy queryById(Integer bId);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    ResponseData queryAllByLimit(Integer offset, Integer limit);
    ResponseData queryByuId(Integer uId);
    /**
     * 新增数据
     *
     * @param buy 实例对象
     * @return 实例对象
     */
    Buy insert(Buy buy);

    /**
     * 修改数据
     *
     * @param buy 实例对象
     * @return 实例对象
     */
    Buy update(Buy buy);

    /**
     * 通过主键删除数据
     *
     * @param bId 主键
     * @return 是否成功
     */
    boolean deleteById(Integer bId);

    ResponseData getCount();

    ResponseData getGoodsCountByDay();

    ResponseData getGoodsCountByMonth();

    ResponseData getGoodsCountByYear();

    ResponseData updateStatus(Integer id, Integer status);

    ResponseData delete(JSONArray arr);


    ResponseData queryByBuy();

    ResponseData updateInfo(Buy buy);
}