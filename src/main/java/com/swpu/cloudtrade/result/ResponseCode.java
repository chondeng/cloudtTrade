package com.swpu.cloudtrade.result;

public enum ResponseCode {
    SUCCESS("200", "操作成功"),
    FAILED("999999", "操作失败"),
    SQL_EXCEPTION("100", "SQL异常"),
    SYSTEM_EXCEPTION("101", "系统异常"),
    USERNAME_EXCEPTION("102", "用户名异常"),
    PASSWORD_EXCEPTION("103", "密码异常"),
    PARAMETER_EXCEPTION("104", "参数异常"),
    AUTHTICATION_EXCEPTION("105", "用户身份不合法"),
    NO_DATA("301", "无数据");

    private String ecode;
    private String errorinfo;

    ResponseCode(String ecode, String errorinfo) {
        this.ecode = ecode;
        this.errorinfo = errorinfo;
    }

    public String getEcode() {
        return ecode;
    }

    public void setEcode(String ecode) {
        this.ecode = ecode;
    }

    public String getErrorinfo() {
        return errorinfo;
    }

    public void setErrorinfo(String errorinfo) {
        this.errorinfo = errorinfo;
    }


}
