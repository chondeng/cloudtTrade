package com.swpu.cloudtrade.result;

public class ResponseData {
    //状态码
    private String ecode;
    //错误信息
    private String errorinfo;
    private Object data;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public ResponseData(ResponseCode responseCode, Object data) {
        this.ecode = responseCode.getEcode();
        this.errorinfo = responseCode.getErrorinfo();
        this.data = data;
    }

    public ResponseData(ResponseCode responseCode) {
        this.ecode = responseCode.getEcode();
        this.errorinfo = responseCode.getErrorinfo();
    }


    public ResponseData(Object data) {
        this.ecode =ResponseCode.SUCCESS.getEcode();
        this.errorinfo = ResponseCode.SUCCESS.getErrorinfo();
        this.data = data;
    }



    public ResponseData(String ecode, String errorinfo, Object data,int count) {
        this.ecode = ecode;
        this.errorinfo = errorinfo;
        this.data = data;
        this.count = count;
    }


    public ResponseData(String ecode, String errorinfo) {
        this.ecode = ecode;
        this.errorinfo = errorinfo;
    }

    public String getEcode() {
        return ecode;
    }

    public void setEcode(String ecode) {
        this.ecode = ecode;
    }

    public String getErrorinfo() {
        return errorinfo;
    }

    public void setErrorinfo(String errorinfo) {
        this.errorinfo = errorinfo;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }




}
