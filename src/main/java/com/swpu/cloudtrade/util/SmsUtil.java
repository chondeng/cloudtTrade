package com.swpu.cloudtrade.util;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.HashMap;
import java.util.UUID;

/*
pom.xml
<dependency>
  <groupId>com.aliyun</groupId>
  <artifactId>aliyun-java-sdk-core</artifactId>
  <version>4.5.16</version>
</dependency>
*/
public class SmsUtil {
    public static void main(String[] args) {
        sendMsg("17844644765");
    }

    public static String sendMsg(String PhoneNumber) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI5tPsQq7pCRQ7ARUnGmQq", "DfgAzFCIEZoSVmA3U9usZHQ9ygLY44");
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("PhoneNumbers", PhoneNumber);
        request.putQueryParameter("SignName", "阳阳商城");
        request.putQueryParameter("TemplateCode", "SMS_205409285");

        //随机生成5位数的验证码
        String code = UUID.randomUUID().toString().substring(0, 5);
        HashMap<String, String> map = new HashMap<>();
        map.put("code", code);

        //验证码的参数Json格式
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));

        try {
            CommonResponse response = client.getCommonResponse(request);

            String data = response.getData();
            // 将json格式字符串转换为Jsonobject对象
            JSONObject jsonObject = JSONObject.parseObject(data);
            String code1 = jsonObject.getString("Code");
            if (code1.equals("OK")) {
                return code;

            }
            return null;

        } catch (ServerException e) {
            e.printStackTrace();
            return null;
        } catch (ClientException e) {
            e.printStackTrace();
            return null;
        }
    }
}
