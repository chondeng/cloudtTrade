package com.swpu.cloudtrade.util;

import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.result.ResponseData;

public class BaseUtil {

    /**
     * 响应返回结果
     *
     * @param rows 影响行数
     * @return 操作结果
     */
    protected ResponseData toAjax(int rows) {
        return rows > 0 ? new ResponseData(ResponseCode.SUCCESS) : new ResponseData(ResponseCode.FAILED);
    }
}
