package com.swpu.cloudtrade.dao;


import com.swpu.cloudtrade.entity.Product;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;


import java.util.List;

/**
 * (Product)表数据库访问层
 *
 * @author makejava
 * @since 2021-07-06 09:48:49
 */
public interface ProductDao  extends   BaseMapper<Product>{

    /**
     * 通过ID查询单条数据
     *
     * @param proId 主键
     * @return 实例对象
     */
    Product queryById(Integer proId);
    List<Product>  queryByuId(Integer uId);
    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Product> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param product 实例对象
     * @return 对象列表
     */
    List<Product> queryByuId(Product product);
    /**
     * 新增数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int insert(Product product);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<Product> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<Product> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<Product> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<Product> entities);

    /**
     * 修改数据
     *
     * @param product 实例对象
     * @return 影响行数
     */
    int update(Product product);

    /**
     * 通过主键删除数据
     *
     * @param proId 主键
     * @return 影响行数
     */
    int deleteById(Integer proId);


    Integer getProCount();

    Integer getProCountByDay(String subtime);

    List<Product> queryByTitle(String title);

    List<Product>getProductByKey(String productName);

    Integer updateStatus(@Param("id") Integer id,@Param("status") Integer status);

    Integer getCount();


}

