package com.swpu.cloudtrade.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.swpu.cloudtrade.entity.Buy;
import com.swpu.cloudtrade.entity.Product;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Buy)表数据库访问层
 *
 * @author makejava
 * @since 2021-07-06 11:14:00
 */
public interface BuyDao extends BaseMapper<Buy> {




    /**
     * 通过ID查询单条数据
     *
     * @param bId 主键
     * @return 实例对象
     */
    Buy queryById(Integer bId);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Buy> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param buy 实例对象
     * @return 对象列表
     */
    List<Buy> queryAll(Buy buy);

    /**
     * 新增数据
     *
     * @param buy 实例对象
     * @return 影响行数
     */
    int insert(Buy buy);

    /**
     * 修改数据
     *
     * @param buy 实例对象
     * @return 影响行数
     */
    int update(Buy buy);

    /**
     * 通过主键删除数据
     *
     * @param bId 主键
     * @return 影响行数
     */
    int deleteById(Integer bId);

    Integer getCount();

    Integer getGoodsCountByDay(String s);

    //需要传参数到sql语句，有两个及以上参数时，不能自动识别，应该为每个参数加标识符
    Integer updateStatus(@Param("id") Integer id,@Param("status") Integer status);

    List<Buy> queryByBuy();

    List<Buy> queryByuId(Integer uId);

    Integer updateInfo(Buy buy);
}