package com.swpu.cloudtrade.interceptor;
import com.alibaba.fastjson.JSONObject;
import com.swpu.cloudtrade.entity.SysUser;
import com.swpu.cloudtrade.result.ResponseCode;
import com.swpu.cloudtrade.service.SysUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: xiaobobo
 * @Date: 2021/7/4 14:44
 * @Description:这个是添加用户身份的拦截器
 *
 * 这个拦截器最主要的功能就是实现用户的身份认证
 *
 */
@Component
public class AuthticationInterceptor implements HandlerInterceptor {

    private Logger logger= LoggerFactory.getLogger(AuthticationInterceptor.class);

    @Autowired
    private SysUserService sysUserService;


    /**
     * 静态资源不需要拦截
     *
     * 因为做的是前后分离 所以这里不涉及到 静态资源
     * 下面这个是可以忽略的
     *
     */
    private static final String[] noStaticInterceptor={
            ".html",".jpg",".jsp",".css",".js"
    };
    /**
     * /sysUser/login.action也不需要拦截
     */
    private static final String[] noActionInterceptor={
            "/sysUser/login.action","/error","/swagger-resources/configuration/ui","/swagger-resources/configuration/security",
            "/swagger-resources",
            "/webjars/springfox-swagger-ui/favicon-32x32.png","/webjars/springfox-swagger-ui/favicon-16x16.png"
            ,"/","/csrf"
    };


    /**
     * false：表示的是请求要拦截
     * true：表示的是放行
     *
     * @param request
     * @param response
     * @param handler
     * @return
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        /**
         * 1、获取当前的请求的URI  http://127.0.0.1:8080/users/picList.action?name=xxxx&fff=123
         *              端口之后  ？ 之前的那一一截    /users/picList.action
         * 2、判断是不是不需要拦截 还是需要拦截
         *     注意:/sysUser/login.action 是不需要拦截的
         * 3、如果不需要拦截 直接放行就可以了
         * 4、如果需要拦截 那么需要获取前端传递过来的token
         * 5、判断这个token在数据库中是否存在
         *    如果是存在 那么 放行
         *    如果不存在 那么说明这个用户的身份不合法  那么就拒绝 并且告诉他身份不合法 请登录
         */
        //第一步：获取这个URI地址
        String requestURI = request.getRequestURI();
        logger.info("请求的URI是:"+requestURI);
        //判断这个URI是不是不需要拦截的?
        //下面首先判断是不是静态资源
        for (String staticPath:noStaticInterceptor) {
            if(requestURI.endsWith(staticPath)){   //以什么结尾
                return true;
            }
        }
        //判断是不是login.action这个请求
        for (String actionPath:noActionInterceptor) {
            if(requestURI.equals(actionPath)){  //说明这个动态资源不需要放行
                return true;
            }
        }
        //如果是程序 执行到这里  那么说明这个资源 既不是静态资源  也不是 不需要拦截的资源 这些资源就需要身份认证了
        //获取前端传递过来的token
        String token = request.getHeader("token");
        //接下来判断这个token在数据库中是否存在
        //接下来就可以进行判断了

        logger.info("token获取到了是:{}",token);

        //接下来应该用token是查询我们的数据库 判断token的合法性
        SysUser sysUser = sysUserService.findUserByToken(token);
        if(null==sysUser){
            logger.error("这个请求被拦截:{}",requestURI);

            JSONObject jsonObject=new JSONObject();
            jsonObject.put("eCode", ResponseCode.AUTHTICATION_EXCEPTION.getEcode());
            jsonObject.put("errorInfo",ResponseCode.AUTHTICATION_EXCEPTION.getErrorinfo());

            response.setContentType("text/html;charset=utf-8");
            response.getWriter().write(jsonObject.toJSONString());
            response.getWriter().flush();

            return false;
        }
        //执行到这里说明身份是合法的
        return true;
    }
}
